package pages;

import static com.codeborne.selenide.Selenide.*;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;


import static tests.BaseTest.config;



public class HomePage {

    private static final By ACCEPT_COOKIES_BUTTON = By.cssSelector("button#onetrust-accept-btn-handler");

    private final static By SEARCH_BAR = By.cssSelector("input[placeholder=\"Куда вы хотите поехать?\"]");

    private static final  By DROPDOWN_CITY_LIST = By.cssSelector("div.a40619bfbe");

    private static final By CALENDAR = By.cssSelector("div.ed2ff9f661");

    private static final By CHECK_IN_DATE = By.cssSelector("span[data-date=\"2023-05-09\"]");

    private static final By CHECK_OUT_DATE = By.cssSelector("span[data-date=\"2023-05-21\"]");

    private static final By SEARCH_BUTTON = By.cssSelector("button[type=\"submit\"]");

    public HomePage openPage() {
        open(config.baseUrl());
        return this;
    }

    public HomePage acceptCookies() {
        $(ACCEPT_COOKIES_BUTTON).click();
        return this;
    }

    public HomePage findByCity(String cityName) {
        $(SEARCH_BAR).sendKeys(cityName);
        $$(DROPDOWN_CITY_LIST).get(0).shouldHave(Condition.text(cityName));
        $(CALENDAR).click();
        $(CHECK_IN_DATE).click();
        $(CHECK_OUT_DATE).click();
        $(SEARCH_BUTTON).click();
        return this;
    }

}
