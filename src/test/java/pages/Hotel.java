package pages;

public class Hotel {

    public String hotelName;

    public String starCount;

    public String rating;

    public String reviewNumber;

    public String price;

    public Hotel (String hotelName, String starCount, String rating, String reviewNumber, String price) {
        this.hotelName=hotelName;
        this.starCount=starCount;
        this.rating=rating;
        this.reviewNumber=reviewNumber;
        this.price=price;
    }


}
