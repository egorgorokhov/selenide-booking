package pages;



import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class HotelPage {

    private static final By HOTEL_NAME = By.cssSelector("h2.pp-header__title");

    private static final By STAR_RATING_FIELD = By.cssSelector("span[data-testid=\"rating-stars\"]");

    private static final By STAR = By.cssSelector("span");

    private static final By RATING = By.cssSelector("div.b5cd09854e.d10a6220b4");

    private static final By REVIEW_NUMBER = By.cssSelector("div.d8eab2cf7f.c90c0a70d3.db63693c62");

    private static final By PRICE = By.cssSelector("span.prco-valign-middle-helper");




    public Hotel getHotelInfo() {
        String hotelNameInPage = $(HOTEL_NAME).getText();
        String starCountInPage =Integer.toString($(STAR_RATING_FIELD).findAll(STAR).size());
        String ratingInPage = $(RATING).getText();
        String reviewNumberInPage = $(REVIEW_NUMBER).getText();
        String priceInPage = $$(PRICE).get(0).getText();

        return new Hotel(hotelNameInPage, starCountInPage, ratingInPage, reviewNumberInPage, priceInPage);
    }
}
