package pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class SearchPage {

    private static final By SORTER_DROPDOWN = By.cssSelector("button[data-testid=\"sorters-dropdown-trigger\"]");

    private static final By SCORE_REVIEW_NUMBER_OPTION = By.cssSelector("button[data-id=\"bayesian_review_score\"]");

    private static final By FREE_CANCELLATION_OPTION = By.xpath("//div[text()=\"Бесплатная отмена\" and @data-testid]");

    private static final By OVERLAY_CARD = By.cssSelector("div[data-testid=\"overlay-card\"]");

    private static final By HOTEL_NAME = By.cssSelector("div[data-testid=\"title\"]");

    private static final By STAR_RATING_FIELD = By.cssSelector("div[data-testid=\"rating-stars\"]");

    private static final By STAR = By.cssSelector("span");

    private static final By RATING = By.cssSelector("div.b5cd09854e.d10a6220b4");

    private static final By REVIEW_NUMBER = By.cssSelector("div.d8eab2cf7f.c90c0a70d3.db63693c62");

    private static final By PRICE = By.cssSelector("span[data-testid=\"price-and-discounted-price\"]");

    public SearchPage setSearchFilters() {

        $(SORTER_DROPDOWN).click();
        $(SCORE_REVIEW_NUMBER_OPTION).click();
        $(FREE_CANCELLATION_OPTION).click();
        $(OVERLAY_CARD).shouldNotBe(Condition.visible);
        return this;
    }

    public Hotel getHotelInfo() {
        String hotelNameInSearch = $$(HOTEL_NAME).get(0).getText();
        String starCountInSearch = Integer.toString($$(STAR_RATING_FIELD).get(0).shouldBe(Condition.exist).findAll(STAR).size());
        String ratingInSearch = $$(RATING).get(0).getText();
        String reviewNumberInSearch = $$(REVIEW_NUMBER).get(0).getText();
        String priceInSearch = $$(PRICE).get(0).getText();

        return new Hotel(hotelNameInSearch, starCountInSearch, ratingInSearch, reviewNumberInSearch, priceInSearch);
    }

    public SearchPage goToHotelPage() {
        $(HOTEL_NAME).click();
        switchTo().window(1);
        return this;
    }
}
