package pages;



import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;


import java.time.Duration;

import static com.codeborne.selenide.Selenide.*;

public class SearchAndMapPage {

    private static final By MAP_BUTTON = By.cssSelector("span.fc63351294");

    private static final By HOTEL_LIST = By.xpath("//a[@class=\"b6ae4fce06\"] | //div[@class=\"map-card__content-container\"]") ;

    private static final By HOTEL_NAME = By.xpath("//span[@data-testid=\"header-title\" or @class=\"map-card__title-link\"]");

    private static final By STAR_RATING_FIELD = By.xpath("//span[@data-testid=\"rating-stars\" or @class=\"c-accommodation-classification-rating\"]");

    private static final By STAR = By.xpath(".//span[@class=\"b6dc9a9e69 adc357e4f1 fe621d6382\" or @class=\"bui-icon bui-rating__item bui-icon--medium\"]");

    private static final By RATING = By.xpath("//div[@class=\"b5cd09854e d10a6220b4\" or @class=\"bui-review-score__badge\"]");

    private static final By REVIEW_NUMBER = By.xpath("//div[@class=\"d8eab2cf7f c90c0a70d3 db63693c62\" or @class=\"bui-review-score__text\"]");

    private static final By PRICE = By.xpath("//span[@class=\"fcab3ed991 bd73d13072\" or @class=\"prco-valign-middle-helper\"]");

    private static final By BOUNCING_MARKER = By.cssSelector("div.bounce");


    public SearchAndMapPage showOnMap() {
        $(MAP_BUTTON).click();
        return this;
    }

    public Hotel getHotelInfo() {
        actions().moveToElement($$(HOTEL_LIST).get(0).shouldBe(Condition.exist, Duration.ofSeconds(10L))).perform();
        String hotelNameInSearch = $$(HOTEL_NAME).get(0).getText();
        String starCountInSearch = Integer.toString($$(STAR_RATING_FIELD).get(0).findAll(STAR).size());
        String ratingInSearch = $$(RATING).get(0).getText();
        String reviewNumberInSearch = $$(REVIEW_NUMBER).get(0).getText();
        String priceInSearch = $$(PRICE).get(0).getText();

        return new Hotel(hotelNameInSearch, starCountInSearch, ratingInSearch, reviewNumberInSearch, priceInSearch);
    }

    public SearchAndMapPage goToHotelPage() {
        actions().moveToElement($$(HOTEL_LIST).get(0)).perform();
        actions().moveToElement($(BOUNCING_MARKER)).click().perform();
        switchTo().window(1);

        return this;
    }
}
