package tests;

import org.junit.Assert;
import org.junit.Test;
import pages.*;



public class SelenideTests extends BaseTest {

    @Test()
    public void firstTest() {

        String cityName = "Токио";

        HomePage homePage = new HomePage();
        SearchAndMapPage searchAndMapPage = new SearchAndMapPage();
        HotelPage hotelPage = new HotelPage();

        homePage.openPage()
                .acceptCookies()
                .findByCity(cityName);
        searchAndMapPage.showOnMap();

        Hotel hotelFromSearch = searchAndMapPage.getHotelInfo();

        searchAndMapPage.goToHotelPage();

        Hotel hotelFromHotelPage = hotelPage.getHotelInfo();

        Assert.assertEquals(hotelFromSearch.hotelName, hotelFromHotelPage.hotelName);
        Assert.assertEquals(hotelFromSearch.starCount, hotelFromHotelPage.starCount);
        Assert.assertEquals(hotelFromSearch.rating, hotelFromHotelPage.rating);
        Assert.assertEquals(hotelFromSearch.reviewNumber, hotelFromHotelPage.reviewNumber);
        Assert.assertEquals(hotelFromSearch.price, hotelFromHotelPage.price);

    }

    @Test()
    public void secondTest() throws InterruptedException {
//        1.1 зайти на сайт https://www.booking.com/
//        1.2 ввести в поиске любой город(заграничный)
//        1.3 выбрать случайные даты
//        1.4 нажать на кнопку «Найти»
//        1.5 нажать на кнопку «Сортировать: Мы рекомендуем для долгой поездки»
//        1.6 выбрать «Оценка + кол-во отзывов»
//        1.7 в фильтрах выбрать "Бесплатная отмена"
//        1.8 сохранить(в переменные) название первого отеля, количество звезд, среднюю оценку, количество отзывов, стоимость
//        1.9 перейти на страницу отеля
//        1.10 на открывшейся странице отеля проверить название, количество звезд, среднюю оценку, количество отзывов, стоимость

        String cityName = "Киото";

        HomePage homePage = new HomePage();
        SearchPage searchPage = new SearchPage();
        HotelPage hotelPage = new HotelPage();

        homePage.openPage()
                .acceptCookies()
                .findByCity(cityName);

        searchPage.setSearchFilters();

        Hotel searchPageHotel = searchPage.getHotelInfo();

        searchPage.goToHotelPage();

        Hotel hotelPageHotel = hotelPage.getHotelInfo();

        Assert.assertEquals(searchPageHotel.hotelName, hotelPageHotel.hotelName);
        Assert.assertEquals(searchPageHotel.starCount, hotelPageHotel.starCount);
        Assert.assertEquals(searchPageHotel.rating, hotelPageHotel.rating);
        Assert.assertEquals(searchPageHotel.reviewNumber, hotelPageHotel.reviewNumber);
        Assert.assertEquals(searchPageHotel.price, hotelPageHotel.price);
    }
}
